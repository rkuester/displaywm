#ifndef IMAGE_H_INCLUDED
#define IMAGE_H_INCLUDED

#include <Ecore_Evas.h>
#include <X11/Xlib.h>

namespace DisplayWM {

class Image
{
    public:
	Image(const char *path);
	~Image();
	void show();
	void hide();
	::Window xwindow_get();

    private:
	Ecore_Evas *ecore_evas;
	Evas *evas;
	Evas_Object *image;
};

}

#endif // IMAGE_H_INCLUDED
