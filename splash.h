#ifndef SPLASH_H_INCLUDED
#define SPLASH_H_INCLUDED

#include <Ecore.h>
#include <boost/signals2.hpp>
#include "image.h"
#include "keyswitch.h"

namespace DisplayWM {

class WindowManager;
class SplashLock;

class Splash
{
    public:
	Splash(WindowManager &, SplashLock &);

    private:
	WindowManager &wm;
	SplashLock &lock;
	Image *splash;
	Keyswitch keyswitch;
	boost::signals2::connection keyswitch_connection;
	boost::signals2::connection lock_connection;

	void enter_splash();
	void exit_splash();
	void enter_inhibited();
	void exit_inhibited();
	void enter_off();
	void exit_off();
	void enter_app();
	void exit_app();

	static Eina_Bool ecore_callback(void *data);
	void reload_splash();
	void ui_off();
	void ui_on();
};

}

#endif // SPLASH_H_INCLUDED
