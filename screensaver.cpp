#include <X11/Xlib.h>
#include "screensaver.h"
using namespace DisplayWM;

ScreenSaver::ScreenSaver(DBus::Connection &c, ::Display *x)
: DBus::ObjectAdaptor(c, "/org/pragmatux/DisplayWm/ScreenSaver")
, xdisplay(x)
{
}

void ScreenSaver::Activate(const std::string &who)
{
	XForceScreenSaver(xdisplay, ScreenSaverActive);
}

void ScreenSaver::Reset(const std::string &who)
{
	XForceScreenSaver(xdisplay, ScreenSaverReset);
}
