#include <X11/Xlib.h>
#include <Ecore_X.h>
#include <iostream>
#include "wm.h"
using namespace DisplayWM;

WindowManager::WindowManager(Display *x)
: xdisplay(x)
, top_window(0)
{
	XSelectInput(
		xdisplay,
		XDefaultRootWindow(xdisplay),
		SubstructureRedirectMask | SubstructureNotifyMask);
	XSync(xdisplay, false);

	frame_existing();

	ecore_event_handler_add(ECORE_X_EVENT_ANY, xevent_callback, this);
}

void WindowManager::frame_existing()
{
	::Window root, parent, *windows;
	::XWindowAttributes attributes;
	unsigned count;

	XGrabServer(xdisplay);

	XQueryTree(
		xdisplay,
		XDefaultRootWindow(xdisplay),
		&root,
		&parent,
		&windows,
		&count);

	for (unsigned i = 0; i < count; ++i) {
		XGetWindowAttributes(
			xdisplay,
			windows[i],
			&attributes);

		if (attributes.map_state == IsViewable)
			frame(windows[i]);
	}

	XFree(windows);

	XUngrabServer(xdisplay);
}

void WindowManager::top_window_set(::Window w)
{
	top_window = w;
}

Eina_Bool WindowManager::xevent_callback(void *data, int type, void *event)
{
	WindowManager *wm = reinterpret_cast<WindowManager*>(data);
	wm->on_xevent(type, event);
	return ECORE_CALLBACK_RENEW;
}

static void print_dbg(const char* c)
{
}

void WindowManager::on_xevent(int type, void *event)
{
	XEvent *ev = reinterpret_cast<XEvent*>(event);

	switch (ev->type) {
	case CreateNotify:
		print_dbg("CreateNotify");
		on_create_notify(ev->xcreatewindow);
		break;

	case DestroyNotify:
		print_dbg("DestroyNotify");
		on_destroy_notify(ev->xdestroywindow);
		break;

	case ReparentNotify:
		print_dbg("ReparentNotify");
		on_reparent_notify(ev->xreparent);
		break;

	case MapNotify:
		print_dbg("MapNotify");
		on_map_notify(ev->xmap);
		break;

	case UnmapNotify:
		print_dbg("UnmapNotify");
		on_unmap_notify(ev->xunmap);
		break;

	case ConfigureNotify:
		print_dbg("ConfigureNotify");
		on_configure_notify(ev->xconfigure);
		break;

	case MapRequest:
		print_dbg("MapRequest");
		on_map_request(ev->xmaprequest);
		break;

	case ConfigureRequest:
		print_dbg("ConfigureRequest");
		on_configure_request(ev->xconfigurerequest);
		break;

	case ButtonPress:
		print_dbg("ButtonPress");
		on_button_press(ev->xbutton);
		break;

	case ButtonRelease:
		print_dbg("ButtonRelease");
		on_button_press(ev->xbutton);
		break;

	case MotionNotify:
		print_dbg("MotionNotify");
		on_motion_notify(ev->xmotion);
		break;

	case KeyPress:
		print_dbg("KeyPress");
		on_key_press(ev->xkey);
		break;

	case KeyRelease:
		print_dbg("KeyRelease");
		on_key_release(ev->xkey);
		break;
	}
}

void WindowManager::on_create_notify(const XCreateWindowEvent& ev)
{
}

void WindowManager::on_destroy_notify(const XDestroyWindowEvent& ev)
{
}

void WindowManager::on_reparent_notify(const XReparentEvent& ev)
{
}

void WindowManager::on_map_notify(const XMapEvent& ev)
{
	focus_top();
}

void WindowManager::on_unmap_notify(const XUnmapEvent& ev)
{
	Window root = XDefaultRootWindow(xdisplay);

	if (windows.count(ev.window) /* we have a frame */ &&
	    ev.event != root /* wasn't a pre-existing, top-level window */)
		unframe(ev.window);

	focus_top();
}

void WindowManager::on_configure_notify(const XConfigureEvent& ev)
{
}

void WindowManager::on_map_request(const XMapRequestEvent& ev)
{
	frame(ev.window);
	XMapWindow(xdisplay, ev.window);
}

void WindowManager::on_configure_request(const XConfigureRequestEvent& ev)
{
	XWindowChanges changes;
	changes.x = ev.x;
	changes.y = ev.y;
	changes.width = ev.width;
	changes.height = ev.height;
	changes.border_width = ev.border_width;
	changes.sibling = ev.above;
	changes.stack_mode = ev.detail;

	XConfigureWindow(xdisplay, ev.window, ev.value_mask, &changes);
}

void WindowManager::on_button_press(const XButtonEvent& ev)
{
}

void WindowManager::on_button_release(const XButtonEvent& ev)
{
}

void WindowManager::on_motion_notify(const XMotionEvent& ev)
{
}

void WindowManager::on_key_press(const XKeyEvent& ev)
{
}

void WindowManager::on_key_release(const XKeyEvent& ev)
{
}

void WindowManager::frame(::Window xwindow)
{
	Window frame = XCreateSimpleWindow(
		xdisplay,
		XDefaultRootWindow(xdisplay),
		0,
		0,
		XDisplayWidth(xdisplay, 0),
		XDisplayHeight(xdisplay, 0),
		0,
		0,
		0);

	XSelectInput(
		xdisplay,
		frame,
		SubstructureRedirectMask | SubstructureNotifyMask);

	XAddToSaveSet(xdisplay, xwindow);

	XReparentWindow(
		xdisplay,
		xwindow,
		frame,
		0,
		0);

	if (top_window) {
		Window stack[] = {top_window, frame};
		XRestackWindows(xdisplay, stack, 2);
	}

	windows[xwindow] = frame;
	XMapWindow(xdisplay, frame);
}

void WindowManager::unframe(::Window xwindow)
{
	Window frame = windows[xwindow];

	XUnmapWindow(xdisplay, frame);

	XReparentWindow(
		xdisplay,
		xwindow,
		XDefaultRootWindow(xdisplay),
		0,
		0);

	XDestroyWindow(
		xdisplay,
		frame);

	windows.erase(xwindow);
}

void WindowManager::focus_top()
{
	::Window root, parent, *frames;
	unsigned count;

	XQueryTree(
		xdisplay,
		XDefaultRootWindow(xdisplay),
		&root,
		&parent,
		&frames,
		&count);

	if (!count)
		return;

	::Window top_frame = frames[0];

	if (!windows.count(top_frame))
		return;

	::Window top_window = windows[top_frame];
	::Window focused;
	int mode;

	XGetInputFocus(xdisplay, &focused, &mode);

	if (top_window != focused)
		XSetInputFocus(xdisplay, top_window, RevertToParent, CurrentTime);
}
