#ifndef WM_H_INCLUDED
#define WM_H_INCLUDED

#include <Ecore.h>
#include <X11/Xlib.h>
#include <unordered_map>

namespace DisplayWM {

class WindowManager
{
    public:
	WindowManager(::Display *xdisplay);
	void top_window_set(::Window);

    private:
	std::unordered_map< ::Window, ::Window> windows;
	Display *xdisplay;
	::Window top_window;

	static Eina_Bool xevent_callback(void *data, int type, void *event);
	void on_xevent(int type, void *event);
	void on_create_notify(const XCreateWindowEvent&);
	void on_destroy_notify(const XDestroyWindowEvent&);
	void on_reparent_notify(const XReparentEvent&);
	void on_map_notify(const XMapEvent&);
	void on_unmap_notify(const XUnmapEvent&);
	void on_configure_notify(const XConfigureEvent&);
	void on_map_request(const XMapRequestEvent&);
	void on_configure_request(const XConfigureRequestEvent&);
	void on_button_press(const XButtonEvent&);
	void on_button_release(const XButtonEvent&);
	void on_motion_notify(const XMotionEvent&);
	void on_key_press(const XKeyEvent&);
	void on_key_release(const XKeyEvent&);

	void frame(::Window);
	void frame_existing();
	void unframe(::Window);
	void restack();
	void focus_top();
};

}

#endif // WM_H_INCLUDED
