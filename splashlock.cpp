#include "splashlock.h"
using namespace DisplayWM;

SplashLock::SplashLock(DBus::Connection &c)
: DBus::ObjectAdaptor(c, "/org/pragmatux/DisplayWm/SplashLock")
, lock(false)
{
}

void SplashLock::LockSplash(const std::string &who)
{
	lock = true;
	locks();
}

void SplashLock::UnlockSplash(const std::string &who)
{
	lock = false;
	unlocks();
}

bool SplashLock::is_locked()
{
	return lock;
}

