#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <ftw.h>
#include <fnmatch.h>
#include <linux/input.h>
#include <algorithm>
#include <iostream>
#include <stdexcept>
#include "keyswitch.h"
using namespace DisplayWM;

static int return_input_with_sw_lid(const char *path, const struct stat *s, int type)
{
	if (type == FTW_F && 
	    fnmatch("/dev/input/event*", path, FNM_PATHNAME) == 0) {
		uint8_t bits[std::max(EV_MAX, SW_MAX)/8 + 1];
		int fd = open(path, O_RDWR | O_NONBLOCK);
		ioctl(fd, EVIOCGBIT(0, EV_MAX), bits);
		if (bits[EV_SW/8] >> EV_SW%8 & 1) {
			ioctl(fd, EVIOCGBIT(EV_SW, SW_MAX), bits);
			if (bits[SW_LID/8] >> SW_LID%8 & 1)
				return fd;
		}

		close(fd);
	}

	return 0;
}

Keyswitch::Keyswitch()
{
	fd = ftw("/dev/input", return_input_with_sw_lid, 1);
	if (fd <= 0)
		throw std::runtime_error("keyswitch input not found");

	ecore_main_fd_handler_add(
		fd, ECORE_FD_READ, fd_ready_callback, this, 0, 0);
}

bool Keyswitch::is_on()
{
	uint8_t switch_bits[SW_MAX/8+1] = {};
	int rc = ::ioctl(fd, EVIOCGSW(SW_MAX), switch_bits);
	if (rc < 0) {
		std::cerr << "EVIOCGSW ioctl failed" << std::endl;
		return true; // failsafe
	}

	bool lid_closed = switch_bits[SW_LID/8] >> SW_LID%8 & 1;
	return !lid_closed;
}

Eina_Bool Keyswitch::fd_ready_callback(void *data, Ecore_Fd_Handler *handler)
{
	Keyswitch *keyswitch = reinterpret_cast<Keyswitch*>(data);
	struct ::input_event ev;
	ssize_t n;

	do {
		n = ::read(keyswitch->fd, &ev, sizeof(ev));
		if (n > 0 && ev.type == EV_SW && ev.code == SW_LID) {
			bool lid_closed = ev.value;
			if (lid_closed)
				keyswitch->goes_off();
			else
				keyswitch->goes_on();
		}
	} while (n > 0);

	return ECORE_CALLBACK_RENEW;
}
