#ifndef SPLASHLOCK_H_INCLUDED
#define SPLASHLOCK_H_INCLUDED

#include "splashlock.adaptor.h"
#include <boost/signals2.hpp>

namespace DisplayWM {

class SplashLock
: public org::pragmatux::DisplayWm::SplashLock1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
{
    public:
	SplashLock(DBus::Connection &c);
	bool is_locked();
	boost::signals2::signal<void()> locks;
	boost::signals2::signal<void()> unlocks;

    private:
	void LockSplash(const std::string &who);
	void UnlockSplash(const std::string &who);

	bool lock;
};

}

#endif // SPLASHLOCK_H_INCLUDED
