#ifndef KEYSWITCH_H_INCLUDED
#define KEYSWITCH_H_INCLUDED

#include <Ecore.h>
#include <boost/signals2.hpp>

namespace DisplayWM {

class Keyswitch
{
    public:
	Keyswitch();
	bool is_on();
	boost::signals2::signal<void()> goes_on;
	boost::signals2::signal<void()> goes_off;

    private:
	int fd;
	static Eina_Bool fd_ready_callback(void *data, Ecore_Fd_Handler*);
	void on_change(bool is_on);
};

}

#endif // KEYSWITCH_H_INCLUDED
