#ifndef SCREENSAVER_H_INCLUDED
#define SCREENSAVER_H_INCLUDED

#include <X11/Xlib.h>
#include "screensaver.adaptor.h"

namespace DisplayWM {

class ScreenSaver
: public org::pragmatux::DisplayWm::ScreenSaver1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
{
    public:
	ScreenSaver(DBus::Connection &c, ::Display *x);

    private:
	void Activate(const std::string &who);
	void Reset(const std::string &who);
	::Display *xdisplay;
};

}

#endif // SCREENSAVER_H_INCLUDED
