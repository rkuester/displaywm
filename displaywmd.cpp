#include <Ecore.h>
#include <Ecore_X.h>
#include <Ecore_Evas.h>
#include <dbus-c++/dbus.h>
#include <dbus-c++/ecore-integration.h>
#include "wm.h"
#include "splashlock.h"
#include "screensaver.h"
#include "splash.h"
using DisplayWM::WindowManager;
using DisplayWM::SplashLock;
using DisplayWM::ScreenSaver;
using DisplayWM::Splash;

int main(int argc, const char **argv)
{
	ecore_x_init(0);
	ecore_evas_init();
	ecore_app_args_set(argc, argv);
	Display *xdisplay = static_cast<Display*>(ecore_x_display_get());

	DBus::Ecore::BusDispatcher dispatcher;
	DBus::default_dispatcher = &dispatcher;
	DBus::Connection dbus = DBus::Connection::SystemBus();

	WindowManager *wm = new WindowManager(xdisplay);
	SplashLock *lock = new SplashLock(dbus);
	Splash *splash = new Splash(*wm, *lock);
	ScreenSaver *saver = new ScreenSaver(dbus, xdisplay);

	dbus.request_name("org.pragmatux.DisplayWm");
	ecore_main_loop_begin();

	delete saver;
	delete splash;
	delete wm;
	ecore_evas_shutdown();
	return 0;
}
