: ${CPP=g++}

objs="displaywm-preview.o image.o"
redo-ifchange $objs
$CPP -g $LDFLAGS -o $3 $objs $(pkg-config --libs ecore --libs ecore-x --libs ecore-evas --libs evas)
