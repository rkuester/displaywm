#include "image.h"
using DisplayWM::Image;

Image::Image(const char *path)
: ecore_evas(0)
, evas(0)
, image(0)
{
	int width, height;

	ecore_evas = ecore_evas_new(0, 0, 0, 0, 0, 0);
	ecore_evas_screen_geometry_get(ecore_evas, 0, 0, &width, &height);
	ecore_evas_resize(ecore_evas, width, height);

	evas = ecore_evas_get(ecore_evas);
	image = evas_object_image_add(evas);
	evas_object_image_file_set(image, path, 0);
	evas_object_image_filled_set(image, true);
	evas_object_resize(image, width, height);

	evas_object_show(image);
}

void Image::show()
{
	ecore_evas_show(ecore_evas);
	ecore_evas_raise(ecore_evas);
}

void Image::hide()
{
	ecore_evas_hide(ecore_evas);
}

::Window Image::xwindow_get()
{
	return ecore_evas_software_x11_window_get(ecore_evas);
}

Image::~Image()
{
	if (ecore_evas)
		ecore_evas_free(ecore_evas);
}
