#include <Ecore_X.h>
#include <X11/Xlib.h>
#include <iostream>
#include <functional>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "splash.h"
#include "splashlock.h"
#include "wm.h"
using namespace DisplayWM;

Splash::Splash(WindowManager &wm, SplashLock &l)
: wm(wm)
, lock(l)
, splash(0)
{
	reload_splash();
	enter_splash();
}

void Splash::enter_splash()
{
	splash->show();
	ui_on();

	ecore_timer_add(5, ecore_callback, new std::function<void()>(
		std::bind(&Splash::exit_splash, this)));
}

void Splash::exit_splash()
{
	if (keyswitch.is_on()) {
		if (lock.is_locked()) {
			enter_inhibited();
		} else {
			enter_app();
		}
	} else {
		enter_off();
	}
}

void Splash::enter_inhibited()
{
	lock_connection = lock.unlocks.connect(std::bind(
		&Splash::exit_inhibited, this));
	keyswitch_connection = keyswitch.goes_off.connect(std::bind(
		&Splash::exit_inhibited, this));
}

void Splash::exit_inhibited()
{
	lock_connection.disconnect();
	keyswitch_connection.disconnect();

	if (keyswitch.is_on()) {
		enter_app();
	} else {
		enter_off();
	}
}

void Splash::enter_off()
{
	ui_off();
	reload_splash();
	splash->show();

	keyswitch_connection = keyswitch.goes_on.connect(std::bind(
		&Splash::exit_off, this));
}

void Splash::exit_off()
{
	keyswitch_connection.disconnect();
	enter_splash();
}

void Splash::enter_app()
{
	splash->hide();

	keyswitch_connection = keyswitch.goes_off.connect(std::bind(
		&Splash::exit_app, this));
}

void Splash::exit_app()
{
	keyswitch_connection.disconnect();
	enter_off();
}

Eina_Bool Splash::ecore_callback(void *data)
{
	auto mem_fn = reinterpret_cast<std::function<void()>*>(data);
	(*mem_fn)();
	delete mem_fn;
	return ECORE_CALLBACK_CANCEL;
}

void Splash::reload_splash()
{
	const char* custom = "/usr/share/displaywm/custom-splash";
	const char* builtin = "/usr/share/displaywm/builtin-splash";
	const char* path;

	if (access(custom, R_OK) == 0)
		path = custom;
	else
		path = builtin;

	if (splash)
		delete splash;

	splash = new Image(path);
	wm.top_window_set(splash->xwindow_get());
}

void Splash::ui_off()
{
	int fd = ::open("/sys/devices/platform/scom.0/ui_enable", O_RDWR);
	if (fd < 0)
		return;

	::write(fd, "0", 1);

	close(fd);
}

void Splash::ui_on()
{
	int fd = ::open("/sys/devices/platform/scom.0/ui_enable", O_RDWR);
	if (fd < 0)
		return;

	::write(fd, "1", 1);

	close(fd);
}
