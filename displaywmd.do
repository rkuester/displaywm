: ${CPP=g++ -std=c++11}

objs="displaywmd.o splash.o splashlock.o image.o keyswitch.o wm.o screensaver.o"
redo-ifchange $objs
$CPP -g $LDFLAGS -o $3 $objs $(pkg-config --libs x11 --libs ecore --libs ecore-x --libs ecore-evas --libs evas --libs dbus-c++-1 --libs dbus-c++-ecore-1)
