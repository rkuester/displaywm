#include <Ecore.h>
#include <Ecore_X.h>
#include <Ecore_Evas.h>
#include <iostream>
#include "image.h"
using DisplayWM::Image;

Eina_Bool timeout_callback(void *data)
{
	ecore_main_loop_quit();
	return ECORE_CALLBACK_DONE;
}

int main(int argc, const char **argv)
{
	int rc = ecore_x_init(0);
	if (!rc) {
		std::cerr << "Cannot connect to X server; check $DISPLAY."
		          << std::endl;
		return 1;
	}
	ecore_evas_init();
	ecore_app_args_set(argc, argv);

	Image *preview = new Image(argv[1]);
	preview->show();
	ecore_timer_add(3, timeout_callback, 0);

	ecore_main_loop_begin();

	ecore_evas_shutdown();
	return 0;
}
